# https://github.com/docker-library/mongo

## Maintained by: [the Docker Community](https://github.com/docker-library/mongo)

This is the Git repo of the [Docker "Official Image"](https://github.com/docker-library/official-images#what-are-official-images) for [`mongo`](https://hub.docker.com/_/mongo/) (not to be confused with any official `mongo` image provided by `mongo` upstream). See [the Docker Hub page](https://hub.docker.com/_/mongo/) for the full readme on how to use this Docker image and for information regarding contributing and issues.

Process to build and deploy mongodb on CRC:

1. Clone mongo git repository from link above
2. Create your own repository in gitlab or git hub
3. Make that new repo the remote for your local repo from the clone
4. Create a image repository in quay.io
5. Specify the Build Trigger to be a push to the git repo you just created
6. Push your local git repo to the remote; this will start a build on quay
7. Create a deployment config in OCP by one of the following:
    1. Run the following command:
        oc new-app \
        -e MONGODB_USER=boosey \
        -e MONGODB_PASSWORD=mongodb \
        -e MONGODB_DATABASE=parking_events \
        -e MONGODB_ADMIN_PASSWORD=mongodb \
        quay.io/boosey_boudreaux/parking-events-store-mongo:latest

    2. Create a DeploymentConfig YAML using the deployment-config.yaml in this repo
